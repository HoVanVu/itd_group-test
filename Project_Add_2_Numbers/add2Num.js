class MyBigNumber {
  constructor(stn1, stn2) {
    this.stn1 = stn1;
    this.stn2 = stn2;
  }
  sum() {
    let Sum = "";
    let plus_num = 0; // số nhớ

    let num1 = this.stn1.split("").reverse();
    let num2 = this.stn2.split("").reverse();
    let len = Math.max(num1.length, num2.length);

    let result = 0;
    for (let i = 0; i < len; i++) {
      if (num1[i] ? false : true) {
        // vượt qua độ dài của chuỗi 1
        result = parseInt(num2[i]) + 0;
      } else if (num2[i] ? false : true) {
        // vượt qua độ dài của chuỗi 1
        result = parseInt(num1[i]) + 0;
      } else {
        result = parseInt(num1[i]) + parseInt(num2[i]);
      }

      Sum = plus_num + (result % 10) + Sum;

      console.log(
        `${num1[i] ? num1[i] : 0} + ${num2[i] ? num2[i] : 0} = ${result} ${
          plus_num == 0 ? " " : "+ " + plus_num
        }, sum = ${Sum}`
      );

      // thay đổi số nhớ
      if (result >= 10) {
        plus_num = 1;
      } else {
        plus_num = 0;
      }
    }

    return Sum;
  }
};

class unitTesting {
  constructor(stn1, stn2, expect) {
    this.stn1 = stn1;
    this.stn2 = stn2;
    this.expect = expect;
  }
  test() {
    let myNum = new MyBigNumber(this.stn1, this.stn2);
    if (myNum.sum() === this.expect) return true;
    else return false;
  }
}

let test_array = [
  { stn1: "123", stn2: "1234", expect: "1357" },
  { stn1: "19349", stn2: "123499", expect: "142848" },
  { stn1: "912399", stn2: "1234", expect: "913633" },
  { stn1: "99", stn2: "9", expect: "108" },
];

test_array.map((item) => {
  let testCase = new unitTesting(item.stn1, item.stn2, item.expect);
  console.log(testCase.test());
});
